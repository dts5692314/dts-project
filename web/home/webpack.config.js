require('@babel/polyfill');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: [
        '@babel/polyfill',
        './App.jsx',
    ],
    output: {
        path: path.join(__dirname, "dist"),
        filename: 'app.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js|\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                      presets: ['@babel/preset-env', '@babel/preset-react'],
                      plugins: ['@babel/transform-runtime'],
                    },
                },
            }
        ],
    },
    devtool: "source-map", 
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
        }),
        new webpack.ProvidePlugin({
            process: 'process/browser',
            // Buffer: ['buffer', 'Buffer'],
        })
    ],
    devServer: {
        port: 3000,
    }
}
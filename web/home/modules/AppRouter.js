/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { Route, Routes, BrowserRouter } from 'react-router-dom';

import MetricChart from './components/MetricChart';
import SignIn from './components/SignIn';
import MetricList from './components/MetricList';


export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" render={() => <Redirect to="/login/" />} />
        <Route path="/login/" element={<SignIn />} />
        <Route path="/login" element={<SignIn />} />
        <Route path="/home/metricList/" element={<MetricList />} />
        <Route path="/home/metricChart/" element={<MetricChart />} />

        <Route path="*" render={() => <Redirect to="/login/" />} />
      </Routes>
    </BrowserRouter>
  );
}
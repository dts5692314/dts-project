import { Button, DatePicker, Form, InputNumber, Modal, Select, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { API_ENDPOINT } from '../constants/apiEndpointConstant';
import { DISTANCE_UNIT_LIST_OPTIONS, TEMPERATURE_UNIT_LIST_OPTIONS, TYPE_LIST, TYPE_LIST_OPTIONS } from '../constants/metricConstant';
import { columns } from '../selectors/metricListSelector';
import axios from 'axios';
const userId = 1;

export default function MetricList(props) {
  const [metric, setMetric] = useState({});
  const [visible, setVisible] = useState(false);
  const [unitListOptions, setUnitListOptions] = useState([]);
  const [metricList, setMetricList] = useState([]);

  const handleAdd = () => {
    setVisible(true);
  }

  const onCancel = () => {
    setVisible(false);
  }

  const onSave = async (values) => {
    const newMetric = {
      ...values,
      userId,
    };

    const { error, data } = await axios({
      method: 'POST',
      url: API_ENDPOINT.ADD_NEW_METRIC,
      data: newMetric,
    });

    setMetric(newMetric);
    setVisible(false);
    // TODO notification
  }

  const handleTypeChange = (value) => {
    const unitListOptions = value === TYPE_LIST.DISTANCE ? DISTANCE_UNIT_LIST_OPTIONS : TEMPERATURE_UNIT_LIST_OPTIONS;
    setUnitListOptions(unitListOptions);
  };

  const [form] = Form.useForm();

  useEffect(() => {
    let ignore = false;

    async function fetchData() {
      const metricResult = await axios({
        method: 'GET',
        url: `${API_ENDPOINT.GET_ALL_METRICS}?userId=${userId}`,
      });

      if (!ignore) {
        const data = metricResult.data.metricList;
        setMetricList(data);
      }
    }

    fetchData();

    return () => { ignore = true; };
  }, [metric]);

  return (
      <div style={{ margin: '20px 30px' }}>
          <Button
              onClick={handleAdd}
              type="primary"
              style={{
              marginBottom: 16,
              }}
          >
              Add data
          </Button>
          <Table columns={columns} dataSource={metricList} />

          <Modal
            visible={visible}
            title="Add a new collection"
            okText="Create"
            cancelText="Cancel"
            onCancel={onCancel}
            onOk={() => {
              form
                .validateFields()
                .then(values => {
                  form.resetFields();
                  onSave(values);
                })
                .catch(info => {
                  console.log('Validate Failed:', info);
                });
            }}
          >
            <Form
              form={form}
              layout="vertical"
              name="form_in_modal"
            >
              <Form.Item
                name="date"
                label="Date"
                rules={[
                  {
                    required: true,
                    message: 'Please select date of metric!',
                  },
                ]}
              >
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>

              <Form.Item
                name="type"
                label="Type"
                rules={[
                  {
                    required: true,
                    message: 'Please select type of metric!',
                  },
                ]}
              >
                <Select
                  onChange={handleTypeChange}
                  options={TYPE_LIST_OPTIONS}
                />
              </Form.Item>
              <Form.Item
                name="unit"
                label="Unit"
                rules={[
                  {
                    required: true,
                    message: 'Please select unit of metric!',
                  },
                ]}
              >
                <Select
                  defaultValue=""
                  options={unitListOptions}
                />
              </Form.Item>
              <Form.Item
                name="value"
                label="Value"
                rules={[
                  {
                    required: true,
                    message: 'Please select unit of metric!',
                  },
                ]}
              >
                <InputNumber style={{ width: '100%' }} />
              </Form.Item>
            </Form>
          </Modal>
      </div>
  );
}

import { Button, DatePicker, Form, InputNumber, Modal, Select, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { API_ENDPOINT } from '../constants/apiEndpointConstant';
import { DISTANCE_UNIT_LIST_OPTIONS, TEMPERATURE_UNIT_LIST_OPTIONS, TYPE_LIST, TYPE_LIST_OPTIONS } from '../constants/metricConstant';
import { columns } from '../selectors/metricListSelector';
const axios = require('axios');
const userId = 1;

export default function MetricChart(props) {
  const [searchOptions, setSearchOptions] = useState({
    type: 'distance',
    unit: 'centimeter',
    fromDate: null,
    toDate: null,
  })
  const [metricList, setMetricList] = useState([]);
  const [unitListOptions, setUnitListOptions] = useState(DISTANCE_UNIT_LIST_OPTIONS);

  const [form] = Form.useForm();

  const handleTypeChange = (value) => {
    const unitListOptions = value === TYPE_LIST.DISTANCE ? DISTANCE_UNIT_LIST_OPTIONS : TEMPERATURE_UNIT_LIST_OPTIONS;
    setUnitListOptions(unitListOptions);
  };

  const onFinish = (values) => {
    const { type, unit, fromDate, toDate } = values;
    setSearchOptions({
      type, unit, fromDate, toDate,
    })
  };

  useEffect(() => {
    let ignore = false;
    const { type, unit, fromDate, toDate } = searchOptions;

    const fromDateStr = fromDate ? new Date(fromDate.$d).toISOString() : '';
    const toDateStr = fromDate ? new Date(toDate.$d).toISOString() : '';

    async function fetchData() {
      const metricResult = await axios({
        method: 'GET',
        url: `${API_ENDPOINT.GET_LIST_METRICS}?userId=${userId}&type=${type}&unit=${unit}&fromDate=${fromDateStr}&toDate=${toDateStr}`, // TODO use query string
      });

      if (!ignore) {
        const data = metricResult.data.metricList;
        setMetricList(data);
      }
    }

    fetchData();

    return () => { ignore = true; };
  }, [searchOptions]);

  return (
      <div style={{ margin: '20px 30px' }}>
        <Form
          form={form}
          layout="inline"
          name="seach-form"
          onFinish={onFinish}
          initialValues={{
            type: 'distance',
            unit: 'centimeter'
          }}
        >
          <Form.Item
            name="type"
            label="Type"
            rules={[
              {
                required: true,
                message: 'Please select type of metric!',
              },
            ]}
            style={{ width: 200 }}
          >
            <Select
              onChange={handleTypeChange}
              options={TYPE_LIST_OPTIONS}
            />
          </Form.Item>
          <Form.Item
            name="unit"
            label="Unit"
            rules={[
              {
                required: true,
                message: 'Please select unit of metric!',
              },
            ]}
            style={{ width: 200 }}
          >
            <Select
              defaultValue=""
              options={unitListOptions}
            />
          </Form.Item>
          <Form.Item
            name="fromDate"
            label="From Date"
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            name="toDate"
            label="To Date"
          >
            <DatePicker />
          </Form.Item>

          <Form.Item>
            <Button
              onClick={() => {}}
              type="primary"
              style={{
              marginBottom: 16,
              }}
              htmlType="submit"
            >
              Search
            </Button>
          </Form.Item>
        </Form>
        <Table columns={columns} dataSource={metricList} />
        <p>TODO add Chart here</p>
      </div>
  );
}

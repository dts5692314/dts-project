export const TYPE_LIST = {
    DISTANCE: 'distance',
    TEMPERATURE: 'temperature',
}

export const TYPE_LIST_OPTIONS = [
    {
        value: TYPE_LIST.DISTANCE,
        label: 'Distance',
    },
    {
        value: TYPE_LIST.TEMPERATURE,
        label: 'Temperature',
    },
];

export const DISTANCE_UNIT_LIST_OPTIONS = [
    {
        value: 'meter',
        label: 'Meter',
    },
    {
        value: 'centimeter',
        label: 'Centieter',
    },
    {
        value: 'inch',
        label: 'Inch',
    },
    {
        value: 'feet',
        label: 'Feet',
    },
    {
        value: 'yards',
        label: 'Yards',
    }
];

export const TEMPERATURE_UNIT_LIST_OPTIONS = [
    {
        value: 'C',
        label: '°C',
    },
    {
        value: 'K',
        label: '°K',
    },
    {
        value: 'F',
        label: '°F',
    },
]
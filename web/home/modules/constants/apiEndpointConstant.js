export const API_ENDPOINT = {
    ADD_NEW_METRIC: 'http://localhost:3333/api/addNewMetric',
    GET_ALL_METRICS: 'http://localhost:3333/api/getAllMetrics',
    GET_LIST_METRICS: 'http://localhost:3333/api/getListMetrics',
}
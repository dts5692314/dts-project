rm -rf package.json 
ln -s ../core/package.json package.json
rm -rf package-lock.json 
ln -s ../core/package-lock.json package-lock.json
rm -rf node_modules
ln -s ../core/node_modules/ node_modules
rm -rf webpack.config.js 
cp ../core/webpack.config.js webpack.config.js

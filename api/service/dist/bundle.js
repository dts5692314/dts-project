/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar express = __webpack_require__(/*! express */ \"express\");\nvar helmet = __webpack_require__(/*! helmet */ \"helmet\");\nvar morgan = __webpack_require__(/*! morgan */ \"morgan\");\nvar cors = __webpack_require__(/*! cors */ \"cors\");\nvar dotenv = __webpack_require__(/*! dotenv */ \"dotenv\");\ndotenv.config();\nvar app = express();\nvar PORT = process.env.PORT || 3333;\napp.use(\"/api\", __webpack_require__(/*! ./src/routes/router */ \"./src/routes/router.js\"));\napp.use(helmet());\napp.use(cors());\napp.use(morgan('tiny'));\napp.use(express.json());\n__webpack_require__(/*! ./src/configs/db.configs */ \"./src/configs/db.configs.js\");\napp.get('*', function (req, res) {\n  res.json({\n    message: \"Hello dev trau\"\n  });\n});\napp.get('/', function (req, res) {\n  res.json({\n    message: \"Hello dev trau\"\n  });\n});\napp.listen(PORT, function () {\n  console.log(\"Server is running on port \".concat(PORT));\n});\n\n//# sourceURL=webpack://nodejs-demo/./index.js?");

/***/ }),

/***/ "./src/configs/db.configs.js":
/*!***********************************!*\
  !*** ./src/configs/db.configs.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\nvar _process$env = process.env,\n  MONGO_HOST = _process$env.MONGO_HOST,\n  MONGO_PORT = _process$env.MONGO_PORT,\n  MONGO_DATABASE = _process$env.MONGO_DATABASE,\n  MONGO_USERNAME = _process$env.MONGO_USERNAME,\n  MONGO_PASSWORD = _process$env.MONGO_PASSWORD;\n\n// const MONGO_URI = `mongodb://${MONGO_USERNAME}: ${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}?authSource=admin`;\nvar MONGO_URI = \"mongodb://\".concat(MONGO_HOST, \":\").concat(MONGO_PORT, \"/\").concat(MONGO_DATABASE);\nmongoose.connect(MONGO_URI, {\n  useCreateIndex: true,\n  useNewUrlParser: true,\n  useFindAndModify: false,\n  useUnifiedTopology: true\n});\nmongoose.connection.on('error', function (err) {\n  console.log('MngoDB connection error.');\n  console.log(err);\n  process.exit();\n});\nmongoose.connection.once('open', function () {\n  console.log(\"Connected to MongoDB: \".concat(MONGO_URI));\n});\n\n//# sourceURL=webpack://nodejs-demo/./src/configs/db.configs.js?");

/***/ }),

/***/ "./src/controllers/todo.controller.js":
/*!********************************************!*\
  !*** ./src/controllers/todo.controller.js ***!
  \********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ \"@babel/runtime/helpers/interopRequireDefault\");\nvar _regenerator = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/regenerator */ \"@babel/runtime/regenerator\"));\nvar _asyncToGenerator2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ \"@babel/runtime/helpers/asyncToGenerator\"));\nvar TodoService = __webpack_require__(/*! ../services/todo.service.js */ \"./src/services/todo.service.js\");\nvar TodoController = {};\nTodoController.getAllTodos = /*#__PURE__*/function () {\n  var _ref = (0, _asyncToGenerator2[\"default\"])( /*#__PURE__*/_regenerator[\"default\"].mark(function _callee(req, res) {\n    var todos;\n    return _regenerator[\"default\"].wrap(function _callee$(_context) {\n      while (1) switch (_context.prev = _context.next) {\n        case 0:\n          _context.prev = 0;\n          _context.next = 3;\n          return TodoService.getAllTodos();\n        case 3:\n          todos = _context.sent;\n          return _context.abrupt(\"return\", res.status(200).json({\n            todos: todos\n          }));\n        case 7:\n          _context.prev = 7;\n          _context.t0 = _context[\"catch\"](0);\n          res.status(400);\n        case 10:\n        case \"end\":\n          return _context.stop();\n      }\n    }, _callee, null, [[0, 7]]);\n  }));\n  return function (_x, _x2) {\n    return _ref.apply(this, arguments);\n  };\n}();\nmodule.exports = TodoController;\n\n//# sourceURL=webpack://nodejs-demo/./src/controllers/todo.controller.js?");

/***/ }),

/***/ "./src/models/todo.model.js":
/*!**********************************!*\
  !*** ./src/models/todo.model.js ***!
  \**********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar mongoose = __webpack_require__(/*! mongoose */ \"mongoose\");\nvar Schema = mongoose.Schema;\nvar ShopeeSchema = Schema({\n  name: String\n});\nvar ShopeeModel = mongoose.model('Shopees', ShopeeSchema, 'products');\nmodule.exports = ShopeeModel;\n\n//# sourceURL=webpack://nodejs-demo/./src/models/todo.model.js?");

/***/ }),

/***/ "./src/routes/router.js":
/*!******************************!*\
  !*** ./src/routes/router.js ***!
  \******************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar express = __webpack_require__(/*! express */ \"express\");\nvar todoRouter = __webpack_require__(/*! ./todo.routes */ \"./src/routes/todo.routes.js\");\nvar apiRoute = express();\napiRoute.use('/', todoRouter);\nmodule.exports = apiRoute;\n\n//# sourceURL=webpack://nodejs-demo/./src/routes/router.js?");

/***/ }),

/***/ "./src/routes/todo.routes.js":
/*!***********************************!*\
  !*** ./src/routes/todo.routes.js ***!
  \***********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar express = __webpack_require__(/*! express */ \"express\");\nvar TodoController = __webpack_require__(/*! ../controllers/todo.controller.js */ \"./src/controllers/todo.controller.js\");\nvar router = express.Router();\nrouter.post(\"/get-all-todos\", function (req, res) {\n  return TodoController.getAllTodos(req, res);\n});\nmodule.exports = router;\n\n//# sourceURL=webpack://nodejs-demo/./src/routes/todo.routes.js?");

/***/ }),

/***/ "./src/services/todo.service.js":
/*!**************************************!*\
  !*** ./src/services/todo.service.js ***!
  \**************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("\n\nvar _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ \"@babel/runtime/helpers/interopRequireDefault\");\nvar _regenerator = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/regenerator */ \"@babel/runtime/regenerator\"));\nvar _asyncToGenerator2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ \"@babel/runtime/helpers/asyncToGenerator\"));\nvar ShopeeModel = __webpack_require__(/*! ../models/todo.model.js */ \"./src/models/todo.model.js\");\nvar ShopeeService = {};\nShopeeService.getAllTodos = /*#__PURE__*/(0, _asyncToGenerator2[\"default\"])( /*#__PURE__*/_regenerator[\"default\"].mark(function _callee() {\n  return _regenerator[\"default\"].wrap(function _callee$(_context) {\n    while (1) switch (_context.prev = _context.next) {\n      case 0:\n        _context.next = 2;\n        return ShopeeModel.find({});\n      case 2:\n        return _context.abrupt(\"return\", _context.sent);\n      case 3:\n      case \"end\":\n        return _context.stop();\n    }\n  }, _callee);\n}));\nmodule.exports = ShopeeService;\n\n//# sourceURL=webpack://nodejs-demo/./src/services/todo.service.js?");

/***/ }),

/***/ "@babel/runtime/helpers/asyncToGenerator":
/*!**********************************************************!*\
  !*** external "@babel/runtime/helpers/asyncToGenerator" ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = require("@babel/runtime/helpers/asyncToGenerator");

/***/ }),

/***/ "@babel/runtime/helpers/interopRequireDefault":
/*!***************************************************************!*\
  !*** external "@babel/runtime/helpers/interopRequireDefault" ***!
  \***************************************************************/
/***/ ((module) => {

module.exports = require("@babel/runtime/helpers/interopRequireDefault");

/***/ }),

/***/ "@babel/runtime/regenerator":
/*!*********************************************!*\
  !*** external "@babel/runtime/regenerator" ***!
  \*********************************************/
/***/ ((module) => {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("cors");

/***/ }),

/***/ "dotenv":
/*!*************************!*\
  !*** external "dotenv" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("dotenv");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("express");

/***/ }),

/***/ "helmet":
/*!*************************!*\
  !*** external "helmet" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("helmet");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ "morgan":
/*!*************************!*\
  !*** external "morgan" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("morgan");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./index.js");
/******/ 	
/******/ })()
;
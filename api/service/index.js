const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const cors = require('cors');
const dotenv = require('dotenv');
var bodyParser = require("body-parser");

dotenv.config();

const app = express();
const  PORT  = process.env.PORT || 3333;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(helmet());
app.use(cors());
app.use(morgan('tiny'))
app.use(express.json());

// app.use((req,res,next)=>{
//     res.setHeader('Access-Control-Allow-Origin','*');
//     res.setHeader('Access-Control-Allow-Methods','GET,POST,PUT,PATCH,DELETE');
//     res.setHeader('Access-Control-Allow-Methods','Content-Type','Authorization');
//     next(); 
// })
app.use("/api", require('./src/routes/router'));

require('./src/configs/db.configs');

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});


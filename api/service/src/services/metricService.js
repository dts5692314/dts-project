const MetricModel = require('../models/metricModel.js');
const { convertValue } = require('../functions/metricFunction');

const MetricService = {};

MetricService.addNewMetric = async (data) => {
   const newMetric = await MetricModel.create({
      ...data,
      date: new Date(data.date),
   });

   return newMetric;
}

MetricService.getAllMetrics = async () => {
   const metricList = await MetricModel.find({}).lean();

   return metricList;
}

MetricService.getListMetrics = async (...condition) => {
   const [userId, fromDate, toDate, type, currentUnit ] = condition;

   let query = {
      userId,
   };
   let dateQuery = {};

   if (!!fromDate) {
      dateQuery.$gte = new Date(new Date(fromDate).setHours(0, 0, 0, 0));
   }

   if (!!toDate) {
      dateQuery.$lte = new Date(new Date(toDate).setHours(23, 59, 59, 999));
   }

   if (type) {
      query.type = type;
   }

   if (!!fromDate || !!toDate) {
      query.date = dateQuery;
   }

   const metricList = await MetricModel.find(query).sort({ date: -1 }).lean();

   const convertedMetricList = convertValue(metricList, type, currentUnit);

   return convertedMetricList;
}

module.exports = MetricService;
const express = require('express');
const metricRouter = require('./metricRouter');

const apiRoute = express();

apiRoute.use('/', metricRouter);

module.exports = apiRoute;
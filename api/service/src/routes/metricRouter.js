const express = require('express');

const MetricController = require('../controllers/metricController.js');

const router = express.Router();

router.get('/healthCheck', (req, res, next) => MetricController.healthCheck(req, res, next));
router.post('/addNewMetric', (req, res, next) => MetricController.addNewMetric(req, res, next));
router.get('/getAllMetrics', (req, res, next) => MetricController.getAllMetrics(req, res, next));
router.get('/getListMetrics', (req, res, next) => MetricController.getListMetrics(req, res, next));

module.exports = router;
const { DISTANCE_CONVERTER, TEMP_CONVERTER } = require('../constants/metricConstant');

const convertValue = (metricList, type, currentUnit) => {
    const upperCaseType = currentUnit.toUpperCase();

    const result = metricList.map(metric => {
        const { unit, value } = metric;

        const upperCaseUnit = unit.toUpperCase();

        if (upperCaseType === upperCaseUnit) {
            return metric;
        }

        const converter = type === 'distance' ? DISTANCE_CONVERTER : TEMP_CONVERTER;
        const converterValue = converter[`${upperCaseUnit}_TO_${upperCaseType}`];

        const convertedValue = (value * converterValue).toFixed(4);

        return {
            ...metric,
            value: convertedValue,
            unit: currentUnit,
        }
    });

    return result;
}

module.exports = {
    convertValue,
}
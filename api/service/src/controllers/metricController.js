const MetricService = require('../services/metricService.js');

const MetricController = {};

MetricController.healthCheck = async (req, res, next) => {
    try {
        return res.status(200).json({ message: "Hi! I'm good!" });
    } catch (error) {
        next(error);
    }
};

MetricController.addNewMetric = async (req, res, next) => {
    try {
        const { body } = req;
        const newMetric = await MetricService.addNewMetric(body);

        return res.status(200).json({ newMetric })
    } catch (error) {
        next(error);
    }
};

MetricController.getAllMetrics = async (req, res, next) => {
    try {
        const { query } = req;
        const { type, userId, fromDate, toDate, unit: currentUnit } = query;

        const metricList = await MetricService.getAllMetrics(userId, fromDate, toDate, type, currentUnit);

        return res.status(200).json({ metricList })
    } catch (error) {
        next(error);
    }
};

MetricController.getListMetrics = async (req, res, next) => {
    try {
        const { query } = req;
        const { type, userId, fromDate, toDate, unit: currentUnit } = query;

        const metricList = await MetricService.getListMetrics(userId, fromDate, toDate, type, currentUnit);

        return res.status(200).json({ metricList })
    } catch (error) {
        next(error);
    }
};

module.exports = MetricController;
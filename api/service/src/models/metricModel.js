const mongoose = require('mongoose');

const { Schema } = mongoose;

const MetricSchema = Schema({
    userId: Number,
    date: Date,
    value: Number,
    type: {
        type: String,
        enum: ['distance', 'temperature'],
    },
    unit: String,
});

const MetricModel = mongoose.model('Metrics', MetricSchema, 'metrics');

module.exports = MetricModel;